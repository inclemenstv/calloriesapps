//
//  ProductTableViewCell.swift
//  Calories 2.0
//
//  Created by Konstantin Pischanskyi on 7/8/19.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weightTextField: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
