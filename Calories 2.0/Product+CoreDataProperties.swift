//
//  Product+CoreDataProperties.swift
//  Calories 2.0
//
//  Created by Konstantin Pischanskyi on 7/8/19.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//
//

import Foundation
import CoreData


extension Product {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Product> {
        return NSFetchRequest<Product>(entityName: "Product")
    }

    @NSManaged public var callories: Double
    @NSManaged public var protein: Double
    @NSManaged public var carbo: Double
    @NSManaged public var fat: Double
    @NSManaged public var name: String?

}
