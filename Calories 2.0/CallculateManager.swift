//
//  CallculateManager.swift
//  Calories 2.0
//
//  Created by Konstantin Pischanskyi on 7/8/19.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

import Foundation
import UIKit



class CallculateManager {
    
    
    init() {}
    
    
    var protein: Double = 0
    var carbo: Double = 0
    var fat: Double = 0
    var call: Double = 0
    var bmr: Double!
    
   
    func  getCall(weightTF: UITextField, heightTF: UITextField, yearsTF: UITextField, activityLevel: Double, gender: Bool, target: Int) -> String {
        var callories: Double = 0
        var proteinBy: Double = 0
        var fatBy: Double = 0
        var carboBy: Double = 0

        if let conWeight = Double(weightTF.text!), let conHeight = Double(heightTF.text!), let conYears = Double(yearsTF.text!), activityLevel != 0, target != 0  {
            let weight = conWeight
            let height = conHeight
            let years = conYears
            switch gender {
            //Мужчины
            case true: 
                let call = (10 * weight + 6.25 * height - 5 * years + 5) * activityLevel
                switch target {
                //Поддержание веса
                case 1: callories = call
                proteinBy = (callories * 0.3) / 4
                carboBy = (callories * 0.5) / 4
                fatBy = (callories * 0.2) / 9
                //Набрать вес
                case 2: callories = call + (call * 0.15)
                proteinBy = (callories * 0.3) / 4
                carboBy = (callories * 0.5) / 4
                fatBy = (callories * 0.2) / 9
                //Похудеть
                case 3: callories = call - (call * 0.15)
                proteinBy = (callories * 0.3) / 4
                carboBy = (callories * 0.5) / 4
                fatBy = (callories * 0.2) / 9
                default: break
                }
            //Женщины
            case false:
                let call = (10 * weight + 6.25 * height - 5 * years - 161) * activityLevel
                switch target {
                //Поддержание веса
                case 1: callories = call
                proteinBy = (callories * 0.3) / 4
                carboBy = (callories * 0.5) / 4
                fatBy = (callories * 0.2) / 9
                //Набрать вес
                case 2: callories = call + (call * 0.15)
                proteinBy = (callories * 0.3) / 4
                carboBy = (callories * 0.5) / 4
                fatBy = (callories * 0.2) / 9
                //Похудеть
                case 3: callories = call - (call * 0.15)
                proteinBy = (callories * 0.3) / 4
                carboBy = (callories * 0.5) / 4
                fatBy = (callories * 0.2) / 9
                default: break
                }
            }
            return "Рекомендуем придерживатся порядка: \(Int(callories)) ккал/день из них: Жиров - \(Int(fatBy))г, Углеводов - \(Int(carboBy))г, Белков - \(Int(proteinBy))г"
            
        } else {
//            let ac = UIAlertController(title: "Что-то пошло не так", message: "Заполните все поля", preferredStyle: .alert)
//            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
//            ac.addAction(ok)
//            present(ac, animated: true, completion: nil)
            return "error"
        }
        
    }
    
    
}
