//
//  DataManager.swift
//  Calories 2.0
//
//  Created by Konstantin Pischanskyi on 7/8/19.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

import Foundation
import CoreData


class DataManager {
    
    
    private init() {}
    
    
  static  func getDataFromFile() {
        
        let fetchRequest: NSFetchRequest<Product> = Product.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "name != nil")
        
        var records = 0
        
        do {
            let count = try context.count(for: fetchRequest)
            records = count
            print("Data is there already?")
        } catch {
            print(error.localizedDescription)
        }
        
        guard records == 0 else { return }
        
        let pathToFile = Bundle.main.path(forResource: "dataProducts", ofType: "plist")
        let dataArray = NSArray(contentsOfFile: pathToFile!)!
        
        
        for dictionary in dataArray {
            let entity = NSEntityDescription.entity(forEntityName: "Product", in: context)
            let product = NSManagedObject(entity: entity!, insertInto: context) as! Product
            
            let productDictionary = dictionary as! NSDictionary
            product.name = productDictionary["name"] as? String
            product.callories = productDictionary["callories"] as? NSNumber as! Double
            product.carbo = productDictionary["carbo"] as? NSNumber as! Double
            product.protein = productDictionary["protein"] as? NSNumber as! Double
            product.fat = productDictionary["fat"] as? NSNumber as! Double
            
            products.insert(product, at: 0)
            
        }
}
}
