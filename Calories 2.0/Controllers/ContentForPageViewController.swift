//
//  ContentForPageViewController.swift
//  Calories 2.0
//
//  Created by Konstantin Pischanskyi on 7/12/19.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

import UIKit

class ContentForPageViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var nextButton: UIButton!
    
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        
        switch index {
        case 0:
            let pageVC = parent as! PageViewController
            pageVC.nextVC(atIndex: index)
        case 1:
            let userDefaults = UserDefaults.standard
            userDefaults.setValue(true, forKey: "wasIntroWatched")
            userDefaults.synchronize()
            dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
    
    
    
    var header = ""
    var subHeader = ""
//    var imageFile = ""
    var index = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


        titleLabel.text = header
        descriptLabel.text = subHeader
//        imageView.image = UIImage(named: imageFile)
        
        pageControl.numberOfPages = 2
        pageControl.currentPage = index

        switch index {
        case 0: nextButton.setTitle("Дальше", for: .normal)
        case 1: nextButton.setTitle("Открыть", for: .normal)
        default:
            break
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
