//
//  CalloriesViewController.swift
//  Calories 2.0
//
//  Created by Konstantin Pischanskyi on 7/13/19.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

import UIKit

class CalloriesViewController: UIViewController {
    
        
    
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var haightLabel: UILabel!
    @IBOutlet weak var yearsLable: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var targetButton: UIButton!
    @IBOutlet weak var activityButton: UIButton!
    @IBOutlet weak var shareButton: UIBarButtonItem!
    
    @IBOutlet weak var weightTextFiled: UITextField!
    @IBOutlet weak var heightTextField: UITextField!
    @IBOutlet weak var yearsTextField: UITextField!
    @IBOutlet weak var genderControl: UISegmentedControl!
    
    var activityViewController: UIActivityViewController? = nil

    
   
    
    
    
    
    
    //MARK: - Variable
    
    var activity: Double = 0
    var weight: Double = 0
    var height: Double = 0
    var years: Double = 0
    var gender: Bool = true
    var target = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.view.insertSubview(blurEffectView, at: 1)
        
        genderControl.addTarget(self, action: #selector(changeSegment(_:)), for:.valueChanged )
    }
    
    //Выбор пола
    @objc func changeSegment(_ sender: UISegmentedControl) {
        if sender == self.genderControl {
            let index = sender.selectedSegmentIndex
            switch index {
            case 0 : gender = true
            case 1 : gender = false
            default: break
            }
        }
}
    
    //Выбор цели
    @IBAction func choiseTarger(_ sender: UIButton) {
        let ac = UIAlertController(title: "Цель", message: "Чего вы хотите достичь?", preferredStyle: .alert)
        let loseWeight = UIAlertAction(title: "Похудеть", style: .default) { (action) in
            self.targetButton.setTitle("Похудеть", for: .normal)
            self.target = 3
        }
        let updateWeight = UIAlertAction(title: "Набрать вес", style: .default) { (action) in
            self.targetButton.setTitle("Набрать вес", for: .normal)
            self.target = 2
        }
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        let defaultValue = UIAlertAction(title: "Поддержание веса", style: .default) { (action) in
            self.targetButton.setTitle("Поддержание веса", for: .normal)
            self.target = 1
        }
        
        ac.addAction(cancel)
        ac.addAction(loseWeight)
        ac.addAction(updateWeight)
        ac.addAction(defaultValue)
        
        present(ac, animated: true, completion: nil)
    }
    
    
    //Выбор уровня активности
    
    @IBAction func choiseActivity(_ sender: UIButton) {
        let ac = UIAlertController(title: "Активность", message: "Ваш уровень активности: Минимальная(Сидячая работа) Слабая(Переодически хожу) Средняя(Много хожу или езжу) Высокая(Тяжелый физический труд)", preferredStyle: .alert)
        let minimum = UIAlertAction(title: "Минимальная", style: .default) { (action) in
            self.activityButton.setTitle("Минимальная", for: .normal)
            self.activity = 1.2
        }
        let weak = UIAlertAction(title: "Слабая", style: .default) { (action) in
            self.activityButton.setTitle("Слабая", for: .normal)
            self.activity = 1.375
            
        }
        let average = UIAlertAction(title: "Средняя", style: .default) { (action) in
            self.activityButton.setTitle("Средняя", for: .normal)
            self.activity = 1.55
            
        }
        let haigh = UIAlertAction(title: "Высокая", style: .default) { (action) in
            self.activityButton.setTitle("Высокая", for: .normal)
            self.activity = 1.725
            
        }
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        ac.addAction(minimum)
        ac.addAction(weak)
        ac.addAction(average)
        ac.addAction(haigh)
        ac.addAction(cancel)
        
        present(ac, animated: true, completion: nil)
    }
    
    // Получить результат
    @IBAction func resultButton(_ sender: UIBarButtonItem) {
        
        let result = CallculateManager()
        if result.getCall(weightTF: weightTextFiled, heightTF: heightTextField, yearsTF: yearsTextField, activityLevel: activity, gender: gender, target: target) == "error"{
            let ac = UIAlertController(title: "Что-то пошло не так", message: "Заполните все поля", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            ac.addAction(ok)
            present(ac, animated: true, completion: nil)
        } else {
            self.resultLabel.text = result.getCall(weightTF: weightTextFiled, heightTF: heightTextField, yearsTF: yearsTextField, activityLevel: activity, gender: gender, target: target)
        }
    }
    
    @IBAction func sharedResut(_ sender: UIBarButtonItem) {
        let text = self.resultLabel.text
        
        if text?.count == 0 {
            let message = "Сначала получите результат"
            let ac = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            let okAC = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            
            ac.addAction(okAC)
            present(ac, animated: true, completion: nil)
        }
        
        self.activityViewController = UIActivityViewController(activityItems: [self.resultLabel.text ?? "nil"], applicationActivities: nil)
        self.present(activityViewController!, animated: true, completion: nil)
    }
    
    
}
