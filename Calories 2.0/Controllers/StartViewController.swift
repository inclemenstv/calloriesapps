//
//  StartViewController.swift
//  Calories 2.0
//
//  Created by Konstantin Pischanskyi on 7/13/19.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let userDefaults = UserDefaults.standard
        let wasIntroWatched = userDefaults.bool(forKey: "wasIntroWatched")
        guard !wasIntroWatched else { return }
        
        if let pageViewController = storyboard?.instantiateViewController(withIdentifier: "pageViewController") as? PageViewController {
            present(pageViewController, animated: true, completion: nil)
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func close(segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func productPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "segueToProduct", sender: nil)
    }
    
    @IBAction func calloriesPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "calloriesSegue", sender: nil)
    }
    
    
    
//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "segueToProduct" {
////            let dvc = segue.destination as! ProductTableViewController
//        }
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
