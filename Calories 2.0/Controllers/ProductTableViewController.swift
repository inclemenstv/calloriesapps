//
//  PoductTableViewController.swift
//  Calories 2.0
//
//  Created by Konstantin Pischanskyi on 7/8/19.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

import UIKit
import CoreData


var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
var products: [Product] = []

var callSum: Double = 0
var allCellsText = [String: String]()

class ProductTableViewController: UITableViewController, UITextFieldDelegate, NSFetchedResultsControllerDelegate {
    
    var searchController : UISearchController!
    var filteredProducts: [Product] = []

    
    func filterContentFor(searchText text: String) {
        filteredProducts = products.filter{ (product) -> Bool in
            return (product.name?.lowercased().contains(text.lowercased()))!
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        DataManager.getDataFromFile()
        
//        //Настройка searchController
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.barTintColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
        searchController.searchBar.tintColor = .black
        tableView.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredProducts.count
        }

        return products.count
    }

    func productToDisplayAt(indexPath: IndexPath) -> Product {
        
        let product: Product
        
        if searchController.isActive && searchController.searchBar.text != "" {
            product = filteredProducts[indexPath.row]
        } else {
            product = products[indexPath.row]
        }
        return product
    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ProductTableViewCell
        
        let product = productToDisplayAt(indexPath: indexPath)
        
        cell.nameLabel.text = product.name
        cell.weightTextField.delegate = self
        cell.weightTextField.text = ""
        
        return cell
    }
 
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
 
}



extension ProductTableViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentFor(searchText:  searchController.searchBar.text!)
        tableView.reloadData()
    }
}


extension ProductTableViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBar.text == "" {
            navigationController?.hidesBarsOnSwipe = false
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        navigationController?.hidesBarsOnSwipe = true
    }
}
