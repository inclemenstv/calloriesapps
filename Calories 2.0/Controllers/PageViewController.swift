//
//  PageViewController.swift
//  Calories 2.0
//
//  Created by Konstantin Pischanskyi on 7/12/19.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController {
    
    
    var titleArray = ["Узнай", "Подбери"]
    var subTitleArray = ["Узнай необходимое потребление каллорий для твоей цели","Подбери свой рацион питания"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        
        if let firstVC = displayViewController(atIndex: 0) {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
        
    }
    

    func displayViewController(atIndex index: Int) -> ContentForPageViewController? {
        guard index >= 0 else  { return nil }
        guard index < titleArray.count else { return nil }
        
        guard let contentVC = storyboard?.instantiateViewController(withIdentifier: "contentForPageViewController") as? ContentForPageViewController else { return nil }
        
    
        contentVC.header = titleArray[index]
        contentVC.subHeader = subTitleArray[index]
        contentVC.index = index
        
        return contentVC
    }
    
    func nextVC(atIndex index: Int) {
        if let contentVC = displayViewController(atIndex: index + 1) {
            setViewControllers([contentVC], direction: .forward, animated: true, completion: nil)
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension PageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! ContentForPageViewController).index
        index -= 1
        
        return displayViewController(atIndex: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! ContentForPageViewController).index
        index += 1
        
        return displayViewController(atIndex: index)
}
}
